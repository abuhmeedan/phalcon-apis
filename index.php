<?php
use Phalcon\Mvc\Micro;

$app = new Micro();

use Phalcon\Loader;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Postgresql as PdoPgsql;
use Phalcon\Http\Response;
use Phalcon\Mvc\Model\Resultset\Simple;
use Phalcon\Http\Request;

// Use Loader() to autoload our model
$loader = new Loader();

$loader->registerNamespaces(
    [
        'Users\Information' => __DIR__ . '/models/',
		'Users\Posts' => __DIR__ . '/models/',
		'Users\Session' => __DIR__ . '/models/',
		'Cars\Information' => __DIR__ . '/models/',
		'Images\aa' => __DIR__ . '/models/',
    ]
);

$loader->register();
$di = new FactoryDefault();
$di->set(
    'db',
    function () {
        return new PdoPgsql(
            [
                'host'     => 'localhost',
                'username' => 'postgres',
                'password' => 'postgres',
                'dbname'   => 'waleed',
            ]
        );
    }
);
// Create and bind the DI to the application
$app = new Micro($di);
$app->get(
    '/users',
    function () use ($app) {
		$response = new Response();
		$usr = new Users\Information\Users();
		$usersInfo= $usr->getAllUsers();
		if($usersInfo!='f') {
			$response->setJsonContent(
				[
					'status' => 'OK',
					'data'   => $usersInfo,
				]
			);
		} else {
			$response->setStatusCode(500, 'Internal Server Error');
		}
		return $response;
	}
);
$app->post(
    '/post/create',
    function () use ($app) {
		$response = new Response();
		$session = new Users\Session\user_session_token();
		$postInfo = $app->request->getPost();
		$user_id= $session->authSession($postInfo['token'],$postInfo['user_id']);
		if($user_id) {
			$post = new Users\Posts\user_posts();
			$title=$postInfo['title'];
			$description=$postInfo['description'];
			$car_maker=$postInfo['car_maker'];
			$extra=$postInfo['extra'];
			$post_id= $post->post($app);
			echo $post_id;
			if($post_id!='f') {
				$response->setStatusCode(201, 'Created');
				$response->setJsonContent(
					[
						'status' => 'OK',
						'data'   => $post_id,
					]
				);
			} elseif ($post_id=='invalidImage') {
				$response->setStatusCode(500, 'invalidImage');
			}
		} else {
			$response->setStatusCode(500, 'Internal Server Error');
		}
		return $response;
    }
);
// USER LOGIN
$app->post(
    '/login',
    function () use ($app) {
		$response = new Response();
		$usr = new Users\Information\Users($app);
		$user_id= $usr->userLogin($app);
		if($user_id!='f') {
			$session = new Users\Session\user_session_token();
			$user_id= $session->genSession($user_id);
			$response->setStatusCode(201, 'Created');
			$response->setJsonContent(
				[
					'status' => 'OK',
					'data'   => $user_id,
				]
			);
		} else {
			$response->setStatusCode(400, 'Wrong email or password');;
		}
		return $response;
    }
);
// register new user
$app->post(
    '/register',
	function () use ($app) {
		$response = new Response();
		$usr = new Users\Information\Users($app);
		$user_id= $usr->createUser($app);
		if($user_id!='f') {
			$response->setStatusCode(201, 'Created');
			$response->setJsonContent(
				[
					'status' => 'OK',
					'data'   => $user_id,
				]
			);
		} else {
			$response->setStatusCode(500, 'Internal Server Error');;
		}
		return $response;
	}
);
// publish post only by admin roles
$app->post(
    '/publish/{id:[0-9]+}',
    function ($id) use ($app) {
		$response = new Response();
		$usr = new Users\Information\Users();
		$session = new Users\Session\user_session_token();
		$userInfo = $app->request->getPost();
		$user_id= $session->authSession($userInfo['token'],$userInfo['user_id']);
		$user_role= $usr->getUserRole($user_id);
		if($user_id && $user_role=='admin') {
			return 'aaaa';
			$post = new Users\Posts\user_posts();
			$post_id= $post->publishPost($id);
		}
		if($post_id>0) {
			$response->setStatusCode(200, 'Created');
			$response->setJsonContent(
				[
					'status' => 'OK',
					'post_id'   => $post_id,
				]
			);
		} else {
			$response->setStatusCode(401, 'Unauthorized Access');;
		}
		return $response;
	}
);
// get all cars
$app->get(
    '/cars',
    function () use ($app) {
		$response = new Response();
		$car = new Cars\Information\cars_makers();
		$allCars= $car->getAllCars();
		if($allCars!='') {
			$response->setStatusCode(201, 'Created');
			$response->setJsonContent(
				[
					'status' => 'OK',
					'data'   => $allCars,
				]
			);
		} else {
			$response->setStatusCode(500, 'Internal Server Error');;
		}
		return $response;
    }
);
// get all posts for user
$app->get(
    '/posts/user/{id:[0-9]+}',
    function ($id) use ($app) {
		$response = new Response();
		$userPost = new Users\Posts\user_posts();
		$userPosts= $userPost->getUserPosts($id);
		if($userPosts!='null') {
			$response->setJsonContent(
				[
					'status' => 'OK',
					'data'   => $userPosts,
				]
			);
		} else {
			$response->setStatusCode(404, 'There is no posts for this user');;
		}
		return $response;
    }
);
// get all posts for car
$app->get(
    '/posts/car/{car_id:[0-9]+}',
    function ($car_id) use ($app) {
		$response = new Response();
		$userPost = new Users\Posts\user_posts();
		$postsByCar= $userPost->getPostsByCar($car_id);
		if($postsByCar!='') {
			$response->setJsonContent(
				[
					'status' => 'OK',
					'data'   => $postsByCar,
				]
			);
		} else {
			$response->setStatusCode(404, 'There is no posts for this car');;
		}
		return $response;
	}
);
$app->handle();
?>
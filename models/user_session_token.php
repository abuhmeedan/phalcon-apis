<?php
namespace Users\Session;
use Phalcon\Mvc\Model;

class user_session_token extends Model
{
	public function initialize($table='user_session_token') {
        $this->setSource($table);
    }
	public function genSession($user_id) {
		$session=new user_session_token();
		$random = new \Phalcon\Security\Random();
		$token= $random->hex(10);
		do {
			$token= $random->hex(10);
			$validToken = user_session_token::findfirst(
				[
					"token = '$token'",
				]
			);
		} while ($validToken!='');
		$session->user_id = $user_id;
		$session->token = $validToken;
		$session->is_valid = 't';
		$session->creation_time ='now()';
		if($session->save()===true) {
			return $token;
		} else {
			return "f";
		}
	}
	public function authSession($token,$user_id) {
		$session=new user_session_token();
		$user = user_session_token::findfirst(
			[
				"token = '$token' and user_id = '$user_id'",
			]
		);
		$user_id=$user->user_id;
		if($user_id) {
			return $user_id;
		} else {
			return 'f';
		}
	}
}
?>
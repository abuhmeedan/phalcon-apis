<?php

namespace Cars\Information;
use Phalcon\Mvc\Model;
class cars_makers extends Model
{
	public function getAllCars() {
		$cars = cars_makers::find();
        $data = [];
		foreach ($cars as $car) {
            $data[] = [
                'car_id'   => $car->car_id,
                'manufacture' => $car->manufacture,
				'country' => $car->country,
            ];
        }
        return json_encode($data);
	}
}
?>
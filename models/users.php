<?php

namespace Users\Information;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Http\Response;
use Phalcon\Http\Request;
use Phalcon\Validation;

use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Callback;
use Phalcon\Validation\Validator\PresenceOf;

class Users extends Model
{
	public function initialize($table='users') {
        $this->setSource($table);
    }
	 public function validation() {
        $validator = new Validation();
        $validator->add(
            'user_name',
            new StringLength(
                ['min'   => ['user_name'=>8]]
            )
        );
        $validator->add(
            'email',
            new Email(
                ['message'   => 'Email not valid']
            )
        );
		$validator->add(
            'email',
            new Callback(
                [
                    'callback'  => function($info){
						$Domain=explode('@', $info->email)[1];
						$validDomains=array('yahoo.com','gmail.com');
                        if(in_array($Domain,$validDomains)){
                            return true;
                        }
                        return false;
                    },
                    'message'   => 'Email SHould be Gmail or Yahoo'
                ]
            )
        );
        $validator->add(
            'email',
            new Uniqueness(
                [
                    'message'   => 'Email already registered'
                ]
            )
        );
        $validator->add(
            'password',
            new StringLength(
                [
                    'min'   => 8,
                    'max'   => 12
                ]
            )
        );
		$validator->add(
            'password',
            new Callback(
                [
                    'callback'  => function($info){
                        $validPass = preg_match('/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/', $info->password);
                        if ($validPass){
                            return true;
                        }
                        return false;
                    },
                    'message'   => 'Password Must contain at least 1 number, one uppercase character and one lowercase character'
                ]
            )
        );
        $response = new Response();
        if(!$this->validate($validator)){
            $messages = $validator->getMessages();
            $errors = [];
            foreach($messages as $message){
                $errors[] = [$message->getMessage()];
            }
            $response->setHeader(400, 'Error while registering new user')
                    ->setJsonContent([
                        'status'    => 'ERROR',
                        'messages'  => $errors
                    ])->send();
            exit;
        }
    }
	public function createUser($app) {
		$user=new Users();
		$userInfo = $app->request->getPost();
		//$user->user_id = '';
		$user->user_name = $userInfo['name'];
		$user->email = $userInfo['email'];
		$user->password =$userInfo['password'];
		//$user->user_role = 'user';
		$user->creation_date = 'now()';
		if($user->save()===true) {
			return $user->user_id;
		} else {
			return 'f';
		}
		return $response;
	}	
	public function getAllUsers() {
		$users = Users::find();
		foreach ($users as $user) {
            $data[] = [
                'id'   => $user->user_id,
                'name' => $user->user_name,
				'email' => $user->email,
				'password' => $user->password,
				'role' => $user->user_role
            ];
        }
        return json_encode($data);
	}
	public function userLogin($app) {
		$user=new Users();
		$loginInfo = $app->request->getPost();
		$user = Users::findfirst(
			[
				"email = '$loginInfo[email]' and password = '$loginInfo[password]'",
			]
		);
		$user_id=$user->user_id;
		if($user_id) {
			return $user_id;
		} else {
			return 'f';
		}
	}
	public function getUserRole($user_id) {
		$users = Users::findfirst(["user_id='$user_id'"]);
		return $users->user_role;
	}
}
?>
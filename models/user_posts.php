<?php

namespace Users\Posts;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Message;
use Phalcon\Mvc\Model\Validator\Uniqueness;
use Phalcon\Mvc\Model\Validator\InclusionIn;
use Phalcon\Validation;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Http\Response;

class user_posts extends Model
{
	public function validation(){
        $validator = new Validation();
        $validator->add(
            'title',
            new StringLength(
                [
                    'min'   => 20,
                    'max'   => 100,
					'message'   => 'Title should be between 20 and 100 characters'
                ]
            )
        );
        $validator->add(
            'description',
            new StringLength(
                [
                    'min'   => 100,
                    'message'   => 'Description should be at least 100 characters'
                ]
            )
        );
        if (!$this->validate($validator)){
            $errors = [];
            foreach ($validator->getMessages() as $message){
                $errors[] = $message->getMessage();
            }
            $response = new Response();
            $response->setHeader(409, 'ERROR')
                    ->setJsonContent(
                        [
                            'message'   => $errors
                        ]
                    )->send();
            exit;
        }
        return true;
    }
	public function Post($app) {
		$postInfo=$app->request->getPost();
		$post=new user_posts();
		if ($app->request->hasFiles() == true) {
			foreach ($app->request->getUploadedFiles() as $file) {
				$uploadedImageName=$file->getName();
				$imageExtension=explode('.',$uploadedImageName)[1];
				$validExtensions=array('jpg','jpeg','png','gif');
				if(!in_array($imageExtension,$validExtensions)){
					return "invalidImage";
				}
				$random = new \Phalcon\Security\Random();
				$fileName=$random->hex(8);
				$file->moveTo('C:\xampp\htdocs\\'.$fileName.'.'.$imageExtension);
				$imageName= $file->getName();
			}
		}
		$post->user_id = $postInfo['user_id'];
		$post->title =  $postInfo['title'];
		$post->description = $postInfo['description'];
		$post->car_maker = $postInfo['car_maker'];
		$post->extra =$postInfo['extra'];;
		if(isset($fileName)) {
			$post->image=$fileName.'.'.$imageExtension;
		}
		if($post->save()===true) {
			return $post->post_id;
		} else {
			return "f";
		}
	}
	public function publishPost($post_id) {
		$post=new user_posts();
		$post = user_posts::findfirst(["post_id='$post_id'"]);
		$post->is_published ='t';
		if($post->save()===true) {
			return $post->post_id;
		} else {
			return "f";
		}
	}
	public function getUserPosts($user_id) {
		$user_posts = user_posts::find(["user_id='$user_id'"]);
		foreach ($user_posts as $post) {
            $data[] = [
                'post_id'   => $post->post_id,
                'title' => $post->title,
				'description' => $post->description,
				'car_maker' => $post->car_maker,
				'extra' => $post->extra,
				'creation_date' => $post->creation_date
            ];
        }
		return json_encode($data);
	}
	public function getPostsByCar($car_id) {
		$user_posts = user_posts::find(["car_maker='$car_id'"]);
		foreach ($user_posts as $post) {
            $data[] = [
                'post_id'   => $post->post_id,
                'title' => $post->title,
				'description' => $post->description,
				'car_maker' => $post->car_maker,
				'extra' => $post->extra,
				'creation_date' => $post->creation_date
            ];
        }
		echo json_encode($data);
	}
	public function getPostsbyExtra($extraInfo) {
		
		$user_posts = user_posts::find($extraInfo);
		var_dump(user_posts);
		foreach ($extraInfo as $condition) {

        }
		//echo json_encode($data);
	}
}
?>